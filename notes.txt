# Παρουσίαση Git

Παρουσιαστής: GreekLUG
Η/Μ: 30/04/2023

## Βασικές ρυθμίσεις τoυ git

git config --global core.editor vim -c 'set textwidth=80 spell spelllang=en_us'
git config --global user.name "GreekLUG"
git config --global user.email "info@greeklug.gr"
